const BASE_URL = 'http://localhost:5000'

export function checkUsername(username) {
    return fetch(`${BASE_URL}/users`)
    .then(res => res.json())
    .then(users => {
        return users.find(user => user.username === username)
    })
}

export function createUsername(username) {
    return fetch(`${BASE_URL}/users`, { 
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({username})
    }).then(res => res.json())
}