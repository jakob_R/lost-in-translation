import styles from './Login.module.css';
import { useState } from 'react';
import HeaderContainer from '../shared/HeaderContainer';
import { TextField, Button } from '@material-ui/core';
import { checkUsername, createUsername } from './LoginAPI';
import { setStorage } from '../utils/storage.js';
import { useHistory } from 'react-router-dom';

function Login() {
    // eslint-disable-next-line    
    const [inputName, setInputName] = useState('');

    const history = useHistory();

    const onNameChange = (event) => {
        setInputName(event.target.value);
    };

    const onLoginClicked = async () => {
        try {
            const foundUser = await checkUsername(inputName);

            if (foundUser) {
                setStorage('_gl-ss', inputName);
                console.log('in on login cliked')
                history.push("/translation");

                return;
            }
            const createdUser = await createUsername(inputName);
            setStorage('_gl-ss', inputName);
            history.push("/translation");

        } catch (e) {
            console.log(e);
        }
    };

    return (
        <>
            <HeaderContainer />
            <main>
                <div className={styles.loginContainer}>
                    <TextField
                        label="Enter your name"
                        onChange={onNameChange}
                        className={styles.inputName}
                    />
                    
                    <Button
                        variant="contained"
                        color="secondary"
                        onClick={onLoginClicked}
                    >
                        Login
                    </Button>
                </div>
            </main>
        </>
    );
}
export default Login;
