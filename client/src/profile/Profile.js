import { useState, useEffect } from 'react';
import { Avatar, Button } from '@material-ui/core';
import HeaderContainer from '../shared/HeaderContainer';
import { Redirect } from 'react-router-dom';
import { getStorage, removeStorage } from '../utils/storage';
import { useHistory } from 'react-router-dom';

import PersonIcon from '@material-ui/icons/Person';
import {
    Card,
} from '@material-ui/core';
import styles from './Profile.module.css';
import { getTranslations } from './ProfileAPI';

function Profile(props) {
    const user = getStorage('_gl-ss');
    const history = useHistory();

    const [translations, setTranslations] = useState();
    useEffect(() => {
        getTranslations()
            .then((translations) => setTranslations(translations))
            .catch((e) => {
                console.log(e.message);
            });
    }, []);

    const onLogoutClicked = () => {
        console.log('logout')
        removeStorage('_gl-ss')
        history.push("/login");

    }

    const isLoggedIn = () => {
        if (user == null) {
            return <Redirect to="/login" />;
        } else {
            return (
                <>
                    <HeaderContainer></HeaderContainer>
                    <main>
                        <div className={styles.profileContainer}>
                            <Avatar>
                                <PersonIcon />
                            </Avatar>
                            <h2>{user}</h2>
                            <Button onClick={onLogoutClicked}>Logout</Button>
                        </div>
                        <h2> Last Translations</h2>
                        {translations
                            ? translations.map((translation) => (
                                  <Card
                                      className={styles.translations}
                                      key={translation.id}
                                  >
                                      {translation.translation}
                                  </Card>
                              ))
                            : null}
                    </main>
                </>
            );
        }
    };
    return isLoggedIn();
}

export default Profile;
