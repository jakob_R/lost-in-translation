const BASE_URL = 'http://localhost:5000'

export function getTranslations() {
    return fetch(`${BASE_URL}/translations`)
        .then((res) => res.json())
        .then((translations) => {
            return translations;
        });
}