import { createContext, useState } from 'react';

export const AppContext = createContext();

export function AppProvider(props) {
    const [user, setUser] = useState();
    return (
        <AppContext.Provider value={[user, setUser]}>
            {props.children}
        </AppContext.Provider>
    );
}
