import './App.css';
import Login from './login/Login';
import Translation from './translation/Translation';
import Profile from './profile/Profile';
import { getStorage } from './utils/storage.js';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
} from 'react-router-dom';
import { useEffect } from 'react';
import { AppProvider } from './AppProvider';

function App() {
    return (
        <AppProvider>
            <Router>
                <Switch>
                    <Route
                        exact
                        path="/"
                        render={() => <Redirect to="/login" />}
                    />
                    <Route exact path="/login" component={Login} />
                    <Route exact path="/translation" component={Translation} />
                    <Route exact path="/profile" component={Profile} />
                </Switch>
            </Router>
        </AppProvider>
    );
}

export default App;
