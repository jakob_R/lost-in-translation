import styles from './HeaderContainer.module.css'

function HeaderContainer(props) {
    return (
        <header className={ styles.HeaderContainer }>
                 <h1 >Lost in translation</h1>

            { props.children }
        </header>
    )
}

export default HeaderContainer