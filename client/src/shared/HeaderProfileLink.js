import {  Avatar } from '@material-ui/core';
import PersonIcon  from '@material-ui/icons/Person';
import { Link } from 'react-router-dom';
import styles from './HeaderProfileLink.module.css'
import { useHistory } from "react-router-dom";

function HeaderProfileLink(props) {
  const history = useHistory();
/*
  const profileLinkClicked = () => {
    history.push("/profile");
  }*/

  return (
    <Link to={'/profile'}>
    <div className={styles.profileLinkContainer} >
      <h5>{props.name}</h5>
        <Avatar>          
          <PersonIcon />
        </Avatar>
        </div>
        </Link>
  );
}
export default HeaderProfileLink;