import { useState } from 'react';
import HeaderContainer from '../shared/HeaderContainer';
import HeaderProfileLink from '../shared/HeaderProfileLink';
import {
    TextField,
    Card,
    Container,
    Button,
    CardHeader,
} from '@material-ui/core';
import { getSignImage } from './SignsImages.js';
import styles from './Translation.module.css';
import { createTranslation } from './TranslationAPI';
import { getStorage } from '../utils/storage';
import { Redirect } from 'react-router-dom';

function Translation() {
    const [text, setText] = useState('');
    const [signs, setSigns] = useState();
    const user = getStorage('_gl-ss');
    const onTextChange = (event) => {
        setText(event.target.value);
    };

    const onTranslateClicked = () => {
        const toTranslate = text.replaceAll(' ', '').split('');
        const signs = toTranslate.map((char, index) =>
            getSignImage(char, index)
        );
        setSigns(signs);
        createTranslation(text);
    };

    const isLoggedIn = () => {
        console.log(user);
        if (user == null) {
            return <Redirect to="/login" />;
        } else {
            return (
                <>
                    <HeaderContainer>
                        <HeaderProfileLink name={user} />
                    </HeaderContainer>
                    <main>
                        <Card className={styles.card}>
                            <TextField
                                id="standard-full-width"
                                label="Enter text"
                                onChange={onTextChange}
                                className={styles.textInput}
                            />
                            <Button
                                variant="contained"
                                color="secondary"
                                onClick={onTranslateClicked}
                            >
                                Translate
                            </Button>
                        </Card>
                        <Card className={styles.card}>
                            <Container className={styles.signsContainer}>
                                {' '}
                                <h2>Translation</h2>
                                {signs ? signs : null}
                            </Container>
                        </Card>
                    </main>
                </>
            );
        }
    };

    return isLoggedIn();
}
export default Translation;
