const BASE_URL = 'http://localhost:5000'

export function createTranslation(translation) {
    return fetch(`${BASE_URL}/translations`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ translation }),
    }).then((res) => res.json());
}