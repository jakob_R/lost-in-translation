/*import SignA from '../assets/individual_signs/a.png'
import SignB from '../assets/individual_signs/b.png'
import SignC from '../assets/individual_signs/c.png'
import SignD from '../assets/individual_signs/d.png'*/
 

export function getSignImage(sign,key){
    return <img src={'./individual_signs/'+sign+'.png'} key={key} alt={'Sign for: '+sign}></img>
    //return <img src={'../../public/individual_signs/'+sign+'.png'} key='key' alt={'Image of sign: '+sign}></img>
}
